# Introduction

**Doppy** is a piece of software that tries to make running large OMNeT++
simulations on a cluster of computers effortless. It does this by utilizing a
Docker Swarm, and running OMNeT++ together with your models in containers on
the hosts.

The name is a portmanteau of the words *Do*cker, OMNeT++ (commonly abbreviated
as *opp*) and *Py*thon.

## Usage

First, you upload your model into Doppy, where it is then built. You can create
a number of *plans* to execute simulations using the built models. Every time
you launch a plan, a new *campaign* is created, consisting of a number of
*jobs*. When all jobs of a campaign are finished, the campaign is over, and you
can download the results of you simulation.

## Architecture

Every instance of Doppy consists of a *manager* and one or more *workers*. All
of them are in a form of Docker containers. It is not necessary to run the
manager container on the Swarm Master, but for the sake of simplicity, it is
often preferred.

The manager runs a number of components:

A Redis server which is used to distribute and track the jobs with the help of
RQ, as well as to store any other information, such as the plans and the logs of
past jobs.

A Flask application to serve the REST API, to host the built-in web-based
frontends, and to provide the models for the workers.

## Model packages

There are two kinds of model packages: *sources* and *binaries*. Most of the
time you only need to care about the first kind, *sources*. This is what you
need to provide (upload) to Doppy. The binary packages are then created by
running make

## Jobs

A job is anything that the Workers do because the Manager told them so. At the
moment this is one of two things: building a source model, or executing a
simulation run.

## Storage

Sharing a storage volume among a number of containers will decrease the amount
of network traffic between the containers. It is advantageous to share a volume
even locally, with the containers running on a single host, and even better if
a common, performant NAS is available on every host.

## Plans

A plan is the rough equivalent of a "Run Configuration" in the OMNeT++ IDE (or
Eclipse in general). Each plan corresponds to a model, and contains information
on how the model should be run, such as the name and path of the executable to
be ran and of the .ini file, the configuration and an optional run filter, and
any miscellaneous command line arguments.

## Campaigns

A campaign is to a plan what a running process is to a binary program.
When a plan is launched, all of it's information is copied to the newly created
campaign. A campaign A number of jobs are queued or processing.

## API

Most functions of Doppy are available using a REST API. Basically a couple of
HTTP endpoints with a well defined behavior and input/output data format.
Most requests and responses are in JSON format, while some of them are
"form-encoded".

## Frontends

Doppy comes with a basic Web based frontend, but based on the API documentation,
you can easily create your own, or integrate it into your own application.
