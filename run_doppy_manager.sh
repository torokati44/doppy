#!/bin/sh

redis-server --bind "0.0.0.0" &

rq-dashboard &

ifconfig
env

git pull

pip3 install flask_cors

rq worker local -n "localworker" -u redis://doppy-manager:6379 --worker-ttl 4 &

export FLASK_APP=restapi.py
export FLASK_DEBUG=1
flask run --host="0.0.0.0" --with-threads
