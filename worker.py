import subprocess
import os
import sys
import shutil
import curator
import requests
import rq
import tarfile

crtr = curator.Curator()


def send_log_message(manager, job, msg):
    requests.post(manager + "/log_line/" + job.id,  msg)


def build_model_job(manager, source_file_id, workdir, make_args):
    # print(options, file=sys.stderr)
    job = rq.get_current_job(rq.get_current_connection())

    sourcedir = crtr.rent_source(job.get_id(), source_file_id)

    p = subprocess.Popen(["make", "MODE=release"] + make_args, cwd=(sourcedir + "/" + workdir + "/"),
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    line = p.stdout.readline()
    while line:
        print(">> " + line.decode("utf-8"), file=sys.stderr)
        send_log_message(manager, job, line)
        line = p.stdout.readline()

    retval = p.wait()

    crtr.give_binary(job.get_id())
    
    return retval


def run_simulation_job(manager, binaryid, workdir, executable, inifile, config, runnumber, otherflags=[]):
    job = rq.get_current_job(rq.get_current_connection())

    send_log_message(manager, job, ">> Executing simulation job " + job.get_id())

    binarydir = crtr.rent_binary(job.get_id(), binaryid)
    command = [executable, inifile, "-c", config, "-r", runnumber] + otherflags

    send_log_message(manager, job, "\n>> Running \"" + ' '.join([(("'" + c + "'") if (' ' in c) else c)
                                            for c in command]))
    send_log_message(manager, job, "\" in " + workdir + "\n")

    p = subprocess.Popen(command, cwd=os.path.join(binarydir, workdir), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    line = p.stdout.readline()
    while line:
        print(line.decode("utf-8"), file=sys.stderr)
        send_log_message(manager, job, line)
        line = p.stdout.readline()

    retval = p.wait()

    send_log_message(manager, job, ">> Done, exit code: " + str(retval))

    send_log_message(manager, job, "\n>> Submitting result fragment to manager...")
    fragment_id = crtr.give_fragment(job.get_id(), workdir)
    send_log_message(manager, job, "\n>> Uploaded, fragment file ID is " + fragment_id + ", job over.")

    return retval


def collect_fragments(manager, campaign_id, input_file_ids):
    job = rq.get_current_job(rq.get_current_connection())

    send_log_message(manager, job, ">> Executing collection job " + job.get_id())

    n = len(input_file_ids)
    for i, input_id in enumerate(input_file_ids):
        send_log_message(manager, job, "\n>> Extracting fragment file " + input_id + " (" + str(i) + "/" + str(n) + ")")
        crtr.rent_fragment(job.get_id(), input_id)

    send_log_message(manager, job, "\n>> Extracted all fragments, packing and uploading...")
    result_id = crtr.give_result(job.get_id(), campaign_id)
    send_log_message(manager, job, "\n>> Uploaded, result file ID is " + result_id + ", job over.")



