import shortuuid
import re
import os
import sys
import worker
import tarfile
from werkzeug.utils import secure_filename

from redis import Redis
from rq import Queue, Worker

DOPPY_MANAGER_DATA_PATH="/var/doppy/manager/"


class Manager:
    def __init__(self):
        self.redis = Redis(decode_responses=True)
        self.default_queue = Queue(connection=self.redis, default_timeout=60 * 60 * 24 * 10)
        self.local_queue = Queue('local', connection=self.redis, default_timeout=60 * 60 * 24 * 10)

    def file_uploaded(self, kind, file, formdata):
        file_id = shortuuid.uuid()
        filename = secure_filename(file.filename)
        location = os.path.join(DOPPY_MANAGER_DATA_PATH, kind, file_id)
        os.makedirs(location)
        file.save(os.path.join(location, filename))

        self.redis.hmset("doppy:files:" + kind + ":" + file_id, {"location": location, "name": filename})

        if 'fragment_of_job' in formdata:
            job_id = formdata['fragment_of_job']
            self.redis.hmset("doppy:jobs:" + job_id, {"fragmentfile": file_id})

    def get_file_name(self, kind, file_id):
        return self.redis.hget("doppy:files:" + kind + ":" + file_id, "name")

    def get_file_path(self, kind, file_id):
        print("kind: " + kind, file=sys.stderr)
        print("file_id: " + file_id, file=sys.stderr)
        info = self.redis.hgetall("doppy:files:" + kind + ":" + file_id)
        return os.path.join(info["location"], info["name"])

    def get_files(self, kind):
        sources = list()
        for k in self.redis.keys("doppy:files:" + kind + ":*"):
            source = self.redis.hgetall(k)
            source["id"] = k.split(":")[-1]
            sources.append(source)
        return sources

    def get_sources(self):
        return self.get_files("sources")

    def get_binaries(self):
        return self.get_files("binaries")

    def get_fragments(self):
        return self.get_files("fragments")

    def get_results(self):
        return self.get_files("results")

    def get_workers(self):
        return [{
            "name": w.name,
            "state": str(w.get_state()),
            "current_job": w.get_current_job_id()
        } for w in Worker.all(self.redis)]

    def get_jobs(self):
        return [{
            "id": job.id,
            "description": job.description,
            "status": job.status
        } for job in self.default_queue.jobs]

    def get_job_log(self, job):
        return {"log": self.redis.get("doppy:logs:" + job)}

    def log_line(self, job, line):
        m = re.search(r".*Event.*Elapsed:.* (\d+). completed.*", line.decode("utf-8"))
        if m is not None:
            self.redis.hset("doppy:jobs:" + job, "progress", float(m.group(1)))

        m = re.search(r".*>> Extracting fragment file .* \((\d+)/(\d+)\).*", line.decode("utf-8"))
        if m is not None:
            self.redis.hset("doppy:jobs:" + job, "progress", float(m.group(1)) / float(m.group(2)) * 50)

        m = re.search(r".*>> Extracted all fragments, packing and uploading.*", line.decode("utf-8"))
        if m is not None:
            self.redis.hset("doppy:jobs:" + job, "progress", 50.0)

        m = re.search(r".*>> Uploaded, result file ID is .*, job over.*", line.decode("utf-8"))
        if m is not None:
            self.redis.hset("doppy:jobs:" + job, "progress", 100.0)

        self.redis.append("doppy:logs:" + job, line)

    def add_plan(self, data):
        plan_id = shortuuid.uuid()
        self.redis.hmset("doppy:plans:" + plan_id, data)

    def update_plan(self, plan_id, data):
        self.redis.hmset("doppy:plans:" + plan_id, data)

    def get_plan(self, plan_id):
        key = "doppy:plans:" + plan_id
        plan = self.redis.hgetall(key)
        plan["id"] = plan_id
        return plan

    def get_plans(self):
        plans = list()
        for k in self.redis.keys("doppy:plans:*"):
            job = self.redis.hgetall(k)
            job["id"] = k.split(":")[-1]
            plans.append(job)
        return plans

    def launch_plan(self, plan_id):
        plan = self.redis.hgetall("doppy:plans:" + plan_id)
        campaign_id = shortuuid.uuid()

        self.redis.hmset("doppy:campaigns:" + campaign_id + ":info", {**plan, "plan": plan_id, "id": campaign_id})

        for rn in plan['runnumbers'].split(","):
            binaryid = plan['binaryid']
            workdir = plan['workdir']
            exec = plan['executable']
            config = plan['configname']
            inifile = plan['inifile']
            j = self.default_queue.enqueue(worker.run_simulation_job, "http://doppy-manager:5000", binaryid, workdir, exec,
                                           inifile, config, rn)
            self.redis.hmset("doppy:jobs:" + j.id, {"type": "run", "campaign": campaign_id, "runnumber": rn})
            self.redis.rpush("doppy:campaigns:" + campaign_id + ":jobs", j.id)

    def get_campaigns(self):
        campaigns = list()
        for k in self.redis.keys("doppy:campaigns:*:info"):
            campaign = self.redis.hgetall(k)
            campaign["id"] = k.split(":")[2]
            campaigns.append(campaign)
        return campaigns

    def get_source_details(self, source_id):
        details = self.redis.hgetall("doppy:files:sources:" + source_id)
        details["id"] = source_id
        return details

    def get_job_history(self):
        jobs = list()
        for k in self.redis.keys("doppy:jobs:*"):
            id = k.split(":")[-1]
            j = self.redis.hgetall(k)
            j["id"] = id
            jobs.append(j)
        return jobs

    def build_model(self, file_id, workdir, make_args):
        # print(options, file=sys.stderr)
        j = self.default_queue.enqueue(worker.build_model_job, "http://doppy-manager:5000", file_id, workdir, make_args, result_ttl=60000)
        self.redis.hmset("doppy:jobs:" + j.id, {"type": "build", "model": file_id})

    def run_simulation(self, model_name, config_name, run_numbers):
        for rn in run_numbers.split(","):
            j = self.default_queue.enqueue(worker.run_simulation_job, "http://doppy-manager:5000", model_name,
                               "./" + model_name, config_name, rn, result_ttl=60000)
            self.redis.hmset("doppy:jobs:" + j.id, {"type": "run", "workdir": model_name, "executable": model_name,
                                                    "config": config_name, "runnumber": rn})

    def collect_results(self, campaign_id):
        key = "doppy:campaigns:" + campaign_id + ":jobs"
        print("key: " + key, file=sys.stderr)
        jobs = self.redis.lrange(key, 0, -1)
        print("jobs: " + str(jobs), file=sys.stderr)

        files = []
        for j in jobs:
            fragmentfile_id = self.redis.hget("doppy:jobs:" + j, "fragmentfile")
            ff = self.get_file_path("fragments", fragmentfile_id)
            files.append(fragmentfile_id)
            print("ff is " + ff, file=sys.stderr)

        j = self.local_queue.enqueue(worker.collect_fragments, "http://doppy-manager:5000", campaign_id, files, result_ttl=60000)

        self.redis.hmset("doppy:jobs:" + j.id, {"type": "collect", "campaign_id": campaign_id})