import worker
import requests
from redis import Redis
from rq import Queue

q = Queue(connection=Redis())

def enq_aloha():
    q.enqueue(worker.run_simulation, '../omnetpp-5.1/samples/aloha', './aloha -c PureAlohaExperiment')

def upload_model(filename, server = '172.17.0.2'):
    requests.post('http://' + server + ':5000/models/', files={'file': open(filename, 'rb')})
