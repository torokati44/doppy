import requests
import tarfile
import urllib.request
import os
import sys
import shutil
import time
import shortuuid
from redis import Redis

def filename_from_attachment_header(content_disposition):
    print("c_d: " + content_disposition, file=sys.stderr)
    print("fn: " + content_disposition.split("; ", 1)[1].split("=", 1)[1], file=sys.stderr)
    return content_disposition.split("; ", 1)[1].split("=", 1)[1]

class Curator:
    manager = "http://doppy-manager:5000"
    dataPath = "/var/doppy/"

    redis = Redis("doppy-manager", decode_responses=True)

    def store_filedir(self, kind, file_id):
        return os.path.join(self.dataPath, "store", kind, file_id)

    def job_workdir(self, job_id):
        return os.path.join(self.dataPath, "work", job_id)

    def rent_source(self, job_id, file_id):
        self.pull_if_needed("sources", file_id)
        return self.extract("sources", file_id, self.job_workdir(job_id))

    def give_binary(self, job_id):
        file_id = self.create("binaries", self.job_workdir(job_id))
        self.push_if_needed("binaries", file_id)
        return file_id

    def rent_binary(self, job_id, file_id):
        self.pull_if_needed("binaries", file_id)
        return self.extract("binaries", file_id, self.job_workdir(job_id))

    def give_fragment(self, job_id, workdir):
        file_id = self.create("fragments", os.path.join(self.job_workdir(job_id), workdir, "results"))
        self.push_if_needed("fragments", file_id, {"fragment_of_job": job_id})
        return file_id

    def rent_fragment(self, job_id, file_id):
        self.pull_if_needed("fragments", file_id)
        return self.extract("fragments", file_id, self.job_workdir(job_id))

    def give_result(self, job_id, campaign_id):
        file_id = self.create("results", self.job_workdir(job_id))

        location = os.path.join(self.dataPath, "store", "results")
        filename = file_id + ".tar.gz"

        self.redis.hmset("doppy:files:" + "results" + ":" + file_id, {"location": location, "name": filename})
        self.redis.hset("doppy:campaigns:" + campaign_id + ":info", "resultfile", file_id)

        return file_id

    def pull_if_needed(self, kind, file_id):
        localdir = self.store_filedir(kind, file_id)

        if os.path.isdir(localdir):
            print("dir " + localdir + " already exists, waiting for it to be non-empty", file=sys.stderr)
            while not os.listdir(localdir):
                print("still empty...", file=sys.stderr)
                time.sleep(1)
            print("done!", file=sys.stderr)
            return

        os.makedirs(localdir)
        print("downloading file " + file_id, file=sys.stderr)

        url = self.manager + "/" + kind + "/" + file_id
        print("using url " + url, file=sys.stderr)
        filename, headers = urllib.request.urlretrieve(url)
        attachmentname = filename_from_attachment_header(headers.get("Content-Disposition"))

        shutil.move(filename, os.path.join(localdir, attachmentname))

    def push_if_needed(self, kind, file_id, data={}):
        archivepath = os.path.join(self.dataPath, "store", kind, file_id + ".tar.gz")

        requests.post(self.manager + "/" + kind + "/",
                      files={'file': (file_id + ".tar.gz", open(archivepath, 'rb'), "application/gzip", {})}, data=data)

    def extract(self, kind, file_id, directory):
        archivedir = os.path.join(self.dataPath, "store", kind, file_id)
        filename = os.listdir(archivedir)[0]
        # print("filename: " + filename, file=sys.stderr)

        archive = tarfile.TarFile.open(os.path.join(archivedir, filename), "r:gz")
        archive.extractall(directory)
        return directory

    def create(self, kind, directory):
        file_id = shortuuid.uuid()
        filepath = os.path.join(self.dataPath, "store", kind, file_id + ".tar.gz")

        os.makedirs(os.path.join(self.dataPath, "store", kind), exist_ok=True)

        archive = tarfile.open(filepath, "w|gz")
        for c in os.listdir(directory):
            archive.add(os.path.join(directory, c), arcname=c)
        archive.close()

        return file_id

