#!/bin/bash

docker service create --name doppy-manager --network doppy-net --publish 5000:5000 --publish 9181:9181 --publish 6379:6379 --mount type=bind,source=/home/attila/projects/doppy,destination=/opt/doppy torokati44/omnetpp-docker /opt/doppy/run_doppy_manager.sh

