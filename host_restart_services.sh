#!/bin/bash

docker service scale doppy-worker=0
sleep 1
docker service scale doppy-manager=0
sleep 1
docker service scale doppy-manager=1
sleep 1
docker service scale doppy-worker=1
