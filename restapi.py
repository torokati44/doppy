from flask import Flask, request, redirect, send_file
from flask_cors import CORS, cross_origin
import rq_dashboard
from os import listdir
from os.path import isfile, join
import sys
import json
import manager


ALLOWED_EXTENSIONS = {'zip', 'tar', 'tgz', 'gz', 'bz2', 'xz'}

app = Flask("manager", static_url_path='/static')
app.config.from_object(rq_dashboard.default_settings)
app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")

mgr = manager.Manager()

CORS(app)


@app.route('/', methods=['GET'])
def index():
    return redirect("static/index.html")


def list_dir_json(dir):
    onlyfiles = [{"name": f.split('.')[0], "file": f}
                 for f in listdir(dir) if isfile(join(dir, f))]
    return json.dumps(onlyfiles)


@app.route('/model_sources.json')
def list_sources_json():
    return json.dumps(mgr.get_sources())


@app.route('/model_binaries.json')
def list_binaries_json():
    return json.dumps(mgr.get_binaries())


@app.route('/fragments.json')
def list_fragments_json():
    return json.dumps(mgr.get_fragments())


@app.route('/results.json')
def list_results_json():
    return json.dumps(mgr.get_results())


@app.route('/workers.json')
def list_workers():
    return json.dumps(mgr.get_workers())


@app.route('/jobs.json')
def list_jobs():
    return json.dumps(mgr.get_jobs())


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/sources/', methods=['POST'])
@app.route('/binaries/', methods=['POST'])
@app.route('/fragments/', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return "no file"
    file = request.files['file']
    # if the user does not select a file, browser still submits a empty part without filename
    if file.filename == '':
        # no selected file
        return "no file"

    if file and allowed_file(file.filename):
        kind = request.path.replace('/', '')
        mgr.file_uploaded(kind, file, request.form)

    return redirect(request.referrer) if ('referrer' in request.headers and ":5000" in request.headers['referrer']) else "ok"


@app.route('/source_details/<sourceId>', methods=['GET'])
def source_details(sourceId):
    return json.dumps(mgr.get_source_details(sourceId))


@app.route('/action/build/', methods=['POST'])
def action_build():
    data = json.loads(request.get_data().decode("utf-8"))
    print(data, file=sys.stderr)
    mgr.build_model(data["source_id"], data["workdir"], data["make_args"])
    return "ok"


@app.route('/build/', methods=['POST'])
def build_model():
    mgr.build_model(request.form['source_id'])
    return redirect(request.referrer)


@app.route('/run/', methods=['POST'])
def run_simulation():
    mgr.run_simulation(request.form['modelname'], request.form['configname'], request.form['runnumbers'])
    return redirect(request.referrer)


@app.route('/log_line/<job>', methods=['POST'])
def log_line(job):
    mgr.log_line(job, request.data)
    return ""


@app.route('/job_history.json', methods=['GET'])
def job_history():
    return json.dumps(mgr.get_job_history())


@app.route('/job_log/<job>', methods=['GET'])
def job_log(job):
    return json.dumps(mgr.get_job_log(job))


@app.route('/plans/', methods=['POST'])
def plans_post():
    return json.dumps(mgr.add_plan(request.get_json()))


@app.route('/plans/<plan_id>/launch', methods=['POST'])
def plans_launch(plan_id):
    mgr.launch_plan(plan_id)
    return json.dumps({"status": "TBD"})


@app.route('/plans/<plan_id>', methods=['GET'])
def plans_get(plan_id):
    return json.dumps(mgr.get_plan(plan_id))


@app.route('/plans/<plan_id>', methods=['PUT'])
def plans_put(plan_id):
    mgr.update_plan(plan_id, request.get_json())
    return "ok"


@app.route('/plans/', methods=['GET'])
def plans_list():
    return json.dumps(mgr.get_plans())


@app.route('/campaigns/', methods=['GET'])
def campaigns_list():
    return json.dumps(mgr.get_campaigns())


@app.route('/collect/<campaign_id>', methods=['GET'])
def collect(campaign_id):
    mgr.collect_results(campaign_id)
    return "ok"


@app.route('/sources/<file_id>', methods=['GET'])
@app.route('/binaries/<file_id>', methods=['GET'])
@app.route('/fragments/<file_id>', methods=['GET'])
@app.route('/results/<file_id>', methods=['GET'])
def serve_file(file_id):
    kind = request.path.split('/', 2)[1]
    return send_file(mgr.get_file_path(kind, file_id), as_attachment=True,
                     attachment_filename=mgr.get_file_name(kind, file_id))
